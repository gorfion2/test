package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@SpringBootApplication
@RestController
public class DemoApplication {

    @Value("${GREETING_PREFIX:null}")
    private String hello;

    @GetMapping("/")
    public ResponseEntity<String> get() {
        Map<String, String> env = System.getenv();
        // Java 8
        //env.forEach((k, v) -> System.out.println(k + ":" + v));

        // Classic way to loop a map
        String envs = "";
        for (Map.Entry<String, String> entry : env.entrySet()) {
            envs = envs + "\n" + entry.getKey() + " : " + entry.getValue();
        }

        return ResponseEntity.ok(envs);
    }

    @PostMapping("/")
    public ResponseEntity<String> get(@RequestBody Test test) {
        Map<String, String> env = System.getenv();
        return ResponseEntity.ok(env.get(test.getKey()));
    }


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
